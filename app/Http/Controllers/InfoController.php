<?php

namespace App\Http\Controllers;

Use Validator;

use Illuminate\Http\Request;

class InfoController extends Controller
{

    /**
     * Define your validation rules in a property in 
     * the controller to reuse the rules.
     */
    protected $validationRules = [
        'name'      => 'required|max:255',
        'lastname'  => 'required|max:255',
        'email'     => 'required|max:60|unique:info|email',
        'phone'     => 'required|regex:(\(\d{2}\)\d{5}\-\d{4})', // valida: (51)9845-0757
        'country'   => 'required|size:2',   // Falta validar dentre uma lista
        'state'     => 'required|size:2',   // Falta validar dentre uma lista
        'zip'       => 'required|max:15'    // Falta validar dentre uma lista
    ];

    protected $states = [
        'AC' => 'Acre',
        'AL' => 'Alagoas',
        'AP' => 'Amapá',
        'AM' => 'Amazonas',
        'BA' => 'Bahia',
        'CE' => 'Ceará',
        'DF' => 'Distrito Federal',
        'ES' => 'Espírito Santo',
        'GO' => 'Goiás',
        'MA' => 'Maranhão',
        'MT' => 'Mato Grosso',
        'MS' => 'Mato Grosso do Sul',
        'MG' => 'Minas Gerais',
        'PA' => 'Pará',
        'PB' => 'Paraíba',
        'PR' => 'Paraná',
        'PE' => 'Pernambuco',
        'PI' => 'Piauí',
        'RJ' => 'Rio de Janeiro',
        'RN' => 'Rio Grande do Norte',
        'RS' => 'Rio Grande do Sul',
        'RO' => 'Rondônia',
        'RR' => 'Roraima',
        'SC' => 'Santa Catarina',
        'SP' => 'São Paulo',
        'SE' => 'Sergipe',
        'TO' => 'Tocantins'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $infos = \App\Info::all();
        return view('list', compact('infos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $form = ['method' => 'post', 'action' => '/info', 'states' => $this->states];
        return view('form', compact('form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->validationRules);

        if ($validator->fails()) {
            // Redirecionar para o form com as mensagens de erro
            return redirect('/info/create')
                        ->withErrors($validator)
                        ->withInput();
        } else {
            // Redirecionar para a tela inicial com mensagemd de sucesso
            // Cria o recurso com as infos informadas
            $info = \App\Info::create($request->all());

            $id = $info->id;
            // Redireciona para formulario com msg de sucesso
            return redirect('/info/'.$id.'/edit')
                        ->with('success', 'Informações salvas com sucesso!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Na verdade, está recirecionando para a tela de edição
        return redirect('/info/' . $id . '/edit'); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Obtém as informações pela ID informada e preenche o formulário de acordo
        $info = \App\Info::findOrFail($id);
        $form = ['method' => 'put', 'action' => "/info/$id", 'states' => $this->states];
        return view('form', compact('info', 'id', 'form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Altera o campo email do validador para nao marcar como duplicado no caso de update no mesmo recurso
        $this->validationRules['email'] = 'required|max:60|unique:info,email,'.$id.'|email';
        
        // Valida
        $validator = Validator::make($request->all(), $this->validationRules);

        if ($validator->fails()) {
            // Redirecionar para o form com as mensagens de erro
            return redirect('/info/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        } else {

            $update = \App\Info::findOrFail($id);

            // Update changed fields only
            foreach ($this->validationRules as $field => $value) {
                if ($request->$field)
                    $update->$field = $request->$field;
            }

            // Salva (Sério, cara!?!?!?)
            $update->save();

            // Redireciona para formulario com msg de sucesso
            return redirect('/info/'.$id.'/edit')
                        ->with('success', 'Informações atualizadas com sucesso!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	\App\Info::destroy($id);
           return redirect('/info/')
                        ->with('success', 'Cadastro deletado com sucesso!');

    }
	

	
	
}
