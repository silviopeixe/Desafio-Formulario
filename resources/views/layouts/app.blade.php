<html>
    <head>
        <title>{{ env('APP_NAME') }} - @yield('title')</title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
		
        @section('navbar')
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
              <a class="navbar-brand" href="/">Inicio</a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav mr-auto">
                      <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Menu
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="/form">Formulário</a>
                          <a class="dropdown-item" href="/info">Lista</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="/spec">Especificação</a>
                        </div>
                      </li>
                  </ul>
                </div>
            </nav>
        @show


        <div class="container">
          <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="{{ asset('img/lion.svg') }}" alt="lion" width="100" height="152">
            <h2>Desafio: formulário</h2>
            <p class="lead">@yield('lead')</p>
          </div>
            @yield('content')
        </div>
    </body>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</html>