<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/info/create');
});

Route::get('/form', function () {
    return redirect('/info/create');
});


Route::resource('/info', 'InfoController');

Route::get('/info/{id}/delete', 'InfoController@destroy');

Route::get('/spec', function () {
    return view('spec');
});