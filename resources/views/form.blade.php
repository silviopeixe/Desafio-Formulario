@extends('layouts.app')

@section('title', 'Formulario')

@section('lead')
Informações dos cadastros podem ser acessada <a href="/info"> neste link</a>.
@endsection

@section('content')
<div class="container">

      <div class="row justify-content-md-center">
 
        <div class="col-md-8 order-md-1">
          <h4 class="mb-3">Informações pessoais</h4>
          <form class="needs-validation" novalidate method="POST" action="{{ $form['action'] }}" enctype="multipart/form-data">

            @method($form['method'])
            @csrf
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">Nome</label>
                <input type="text" class="form-control" name="name" placeholder="">
                <div class="invalid-feedback">
                  Você deve informar um nome válido.
                </div>
                <div class="valid-feedback">
                    OK!
                </div>
              </div>
			  
              <div class="col-md-6 mb-3">
                <label for="lastname">Sobrenome</label>
                <input type="text" class="form-control" name="lastname" placeholder="" value="{{ $info->lastname or '' }}" required>
                <div class="invalid-feedback">
                  Um sobrenome válido é obrigatório.
                </div>
                <div class="valid-feedback">
                    OK!
                </div>
              </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-3">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" name="email" placeholder="nome@gmail.com" value="{{ $info->email or '' }}" required>
                  <div class="invalid-feedback">
                    Insira um e-mail válido, por favor.
                  </div>
                <div class="valid-feedback">
                    OK!
                </div>
                </div>

                <div class="col-md-6 mb-3">
                  <label for="phone">Telefone</label>
                  <input type="tel" class="form-control" placeholder="Somente números (10 digitos)" name="phone" pattern="\(\d{2}\)\d{5}-\d{4}" data-inputmask="'mask': '(99)99999-9999'" value="{{ $info->phone or '' }}" required>
                  <div class="invalid-feedback">
                    Insira um telefone válido, por favor.
                  </div>
                    <div class="valid-feedback">
                        OK!
                    </div>
                </div>
            </div>

            <div class="row">
              <div class="col-md-5 mb-3">
                <label for="country">País</label>
                <select class="custom-select d-block w-100" name="country" required>
                  <option value="br">Brasil</option>
                </select>
                <div class="invalid-feedback">
                  Por favor, selecione um país válido.
                </div>
              </div>
              <div class="col-md-4 mb-3">
                <label for="state">Estado</label>
                <select class="custom-select d-block w-100" name="state" required>
                    <option value="">Escolha...</option>
                    @php
                        foreach($form['states'] as $key => $val) {
                            $selected = ( isset($info) && $info->state == $key) ? 'selected' : '';
                            echo '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
                        }
                    @endphp
                </select>
                <div class="invalid-feedback">
                  Informe um estado válido.
                </div>
                <div class="valid-feedback">
                    OK!
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <label for="zip">CEP</label>
                <input type="text" class="form-control" id ="cep" name="cep" placeholder="" pattern="\d{4}-\d{3}" data-inputmask="'mask': '9999-999'" value="{{ $info->cep or '' }}" required>
                <div class="invalid-feedback">
                  CEP é obrigatório.
                </div>
                <div class="valid-feedback">
                    OK!
                </div>
              </div>
            </div>

            <hr class="mb-4">

            <button class="btn btn-primary btn-lg btn-block" type="submit">Salvar</button>

            @if ($errors->any())
            <hr class="mb-4">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                 <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
            @endif

            @if (Session::has('success'))
            <hr class="mb-4">
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                <p>{{ Session::get('success') }}</p>
	                @php
	                Session::forget('success');
	                @endphp
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
              </div><br />
             @endif

          </form>
        </div>
      </div>

    <script>
   
    </script>
@endsection